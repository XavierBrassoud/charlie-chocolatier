# Mini projet web : Charlie le Chocolatier

Ce projet contient l'ensemble des fichiers sources pour le projet web demandé en formation L3 TRI.

## Cahier des charges

Site de vente en ligne de chocolats
 * Ce site devra avoir au moins une 30 aine de produits, répertoriés en diverses catégories, avec une photo, un descriptif et un prix
 * Ces produits doivent être stockés dans une base de données
 * Chacun de ces produits pouvant avoir des accessoires (boite en or, coffret anniversaire)
 * Vous proposerez des packs de différents produits ainsi que la possibilité d’avoir des réductions

L’auteur et la date de publication doivent apparaître sur chacune des pages.

Votre site Web doit avoir un menu en CSS, une page contact fonctionnelle, une page à propos qui décrit votre site Web, qui vous êtes, pourquoi vous avez fait ce site, etc.

La qualité du code sera l’un des critères les plus importants :
Vous n’avez droit qu’aux frameworks du cours (Bootstrap pour le CSS, Express.JS et Swig JS).


## Environnement de développement

### Prérequis
 * Docker
 * Node

### Mise en place de l'environnement

Exécutez docker-compose dans le dossier `docker/` :
``` bash
docker-compose up -d
```

Cette configuration va monter deux containers :
* Une base de données MariaDB
* Une interface PhpMyAdmin sur le port 8080

Les containers sont accessible sur votre adresse IP Docker (par défaut 192.168.99.100).

Puis lancez le serveur web :
``` bash
npm run watch
```

Le site web est servi sur http://127.0.0.1:3001
