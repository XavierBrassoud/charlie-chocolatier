#!/bin/bash

RET=1
mysql -u root -p${MYSQL_ROOT_PASSWORD} -e "status" > /dev/null 2>&1
RET=$?
while [[ RET -ne 0 ]]; do
    echo "=> Waiting for confirmation of MariaDB service startup"
    sleep 1
    mysql -u root -p${MYSQL_ROOT_PASSWORD} -e "status" > /dev/null 2>&1
    RET=$?
done

echo "=> Importing database 'chocolaterie'"
mysql -u root -p${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE chocolaterie"
mysql -u root -p${MYSQL_ROOT_PASSWORD} chocolaterie < /docker-entrypoint-initdb.d/db/bdd.sql

echo "=> Done!"
