/* ENCODAGE */
SET names utf8;

/* CRÉATION DES TABLES */
CREATE TABLE Chocolat 
(
    IdChocolat INT NOT NULL AUTO_INCREMENT,
    Nom VARCHAR(255),
    Description VARCHAR(1023),
    UrlImage VARCHAR(1023),
    Prix FLOAT,
    EstFavori BOOLEAN,
    Reduction INT,

    PRIMARY KEY (IdChocolat)
);

CREATE TABLE Categorie 
(
    IdCategorie INT NOT NULL AUTO_INCREMENT,
    Label VARCHAR(255),

    PRIMARY KEY (IdCategorie)
);

CREATE TABLE Accessoire 
(
    IdAccessoire INT NOT NULL AUTO_INCREMENT,
    Label VARCHAR(255),

    PRIMARY KEY (IdAccessoire)
);

CREATE TABLE Assortissement 
(
    IdAssortissement INT NOT NULL AUTO_INCREMENT,
    Label VARCHAR(255),
    Description VARCHAR(1023),
    UrlImage VARCHAR(1023),
    Prix FLOAT,

    PRIMARY KEY (IdAssortissement)
);

CREATE TABLE ChocolatCategorie 
(
    IdChocolat INT,
    IdCategorie INT,

    PRIMARY KEY (IdChocolat, IdCategorie), 
    FOREIGN KEY (IdChocolat) REFERENCES Chocolat (IdChocolat),
    FOREIGN KEY (IdCategorie) REFERENCES Categorie (IdCategorie)
);

CREATE TABLE ChocolatAccessoire 
(
    IdChocolat INT,
    IdAccessoire INT,

    PRIMARY KEY (IdChocolat, IdAccessoire), 
    FOREIGN KEY (IdChocolat) REFERENCES Chocolat (IdChocolat),
    FOREIGN KEY (IdAccessoire) REFERENCES Accessoire (IdAccessoire)
);

CREATE TABLE ChocolatAssortissement 
(
    IdChocolat INT,
    IdAssortissement INT,
    Quantite INT,

    PRIMARY KEY (IdChocolat, IdAssortissement), 
    FOREIGN KEY (IdChocolat) REFERENCES Chocolat (IdChocolat),
    FOREIGN KEY (IdAssortissement) REFERENCES Assortissement (IdAssortissement)
);


/* INSERTION DES DONNÉES */
/* TODO : Réductions ! Ajouter un champ pourcentage de réduction */
INSERT INTO Chocolat (Nom, Description, UrlImage, Prix, EstFavori, Reduction) 
VALUES 
    ('Amandéus Noir', 'Un cailloux fondant...', '/img/individuel/amandéus-noir.jpeg', 0.50, false, 0),
    ('Ballotin Noir', 'Un cailloux fondant...', '/img/individuel/ballotin-noir.jpeg', 0.50, true, 0),
    ('Biscuitine Blanc', 'Un cailloux fondant...', '/img/individuel/biscuitine-blanc.jpeg', 0.50, false, 0),
    ('Bloc Gianduja Lait', 'Un cailloux fondant...', '/img/individuel/bloc gianduja-lait.jpeg', 0.50, false, 10),
    ('Bruges Noir', 'Un cailloux fondant...', '/img/individuel/bruges-noir.jpeg', 0.50, false, 0),
    ('Bruxelles Noir', 'Un cailloux fondant...', '/img/individuel/bruxelles-noir.jpeg', 0.50, false, 20),
    ('Buchette Lait', 'Un cailloux fondant...', '/img/individuel/buchette-lait.jpeg', 0.50, true, 0),
    ('Cornet Doré Lait', 'Un cailloux fondant...', '/img/individuel/cornet doré-lait.jpeg', 0.50, false, 0),
    ('Dentelle Lait', 'Un cailloux fondant...', '/img/individuel/dentelle-lait.jpeg', 0.50, false, 0),
    ('Ecorce au chocolat Blanc', 'Un cailloux fondant...', '/img/individuel/ecorce-au-chocolat-blanc.jpeg', 0.50, false, 0),
    ('Feuillantine Lait', 'Un cailloux fondant...', '/img/individuel/feuillantine-lait.jpeg', 0.50, false, 0),
    ('Gantois Noir', 'Un cailloux fondant...', '/img/individuel/gantois-noir.jpeg', 0.50, true, 30),
    ('Jeff Lait', 'Un cailloux fondant...', '/img/individuel/jeff-lait.jpeg', 0.50, false, 0),
    ('Lait amande miel Lait', 'Un cailloux fondant...', '/img/individuel/lait-amande miel-lait.jpeg', 0.50, false, 0),
    ('Liégeois Blanc', 'Un cailloux fondant...', '/img/individuel/liégeois-blanc.jpeg', 0.50, false, 0),
    ('Maison de Jeff Blanc', 'Un cailloux fondant...', '/img/individuel/maison de jeff-blanc.jpeg', 0.50, false, 10),
    ('Manon Blanc', 'Un cailloux fondant...', '/img/individuel/manon-blanc.jpeg', 0.50, false, 0),
    ('Mini rocher Blanc', 'Un cailloux fondant...', '/img/individuel/mini-rocher-blanc.jpeg', 0.50, false, 0),
    ('Mini rocher Noir', 'Un cailloux fondant...', '/img/individuel/mini-rocher-noir.jpeg', 0.50, true, 0),
    ('Noir Orange Noir', 'Un cailloux fondant...', '/img/individuel/noir-orange-noir.jpeg', 0.50, false, 0),
    ('Nuttine Lait', 'Un cailloux fondant...', '/img/individuel/nuttine-lait.jpeg', 0.50, false, 30),
    ('Orphé Noir', 'Un cailloux fondant...', '/img/individuel/orphé-noir.jpeg', 0.50, false, 0),
    ('Palet jb Blanc', 'Un cailloux fondant...', '/img/individuel/palet-jb-blanc.jpeg', 0.50, true, 10),
    ('Palet jb Lait', 'Un cailloux fondant...', '/img/individuel/palet-jb-lait.jpeg', 0.50, false, 0),
    ('Petite meringue café Noir', 'Un cailloux fondant...', '/img/individuel/petite-meringue-café-noir.jpeg', 0.50, false, 0),
    ('Petite meringue café Lait', 'Un cailloux fondant...', '/img/individuel/petite-meringue-mangue-lait.jpeg', 0.50, false, 0),
    ('Sévillana Noir', 'Un cailloux fondant...', '/img/individuel/sévillana-noir.jpeg', 0.50, false, 40),
    ('Trèfle Noir', 'Un cailloux fondant...', '/img/individuel/trefle-noir.jpeg', 0.50, false, 0),
    ('Vanillon Blanc', 'Un cailloux fondant...', '/img/individuel/vanillon-blanc.jpeg', 0.50, false, 0),
    ('Warren Lait', 'Un cailloux fondant...', '/img/individuel/warren-lait.jpeg', 0.50, false, 0);

INSERT INTO Categorie (Label) 
VALUES 
    ('Blanc'),
    ('Lait'),
    ('Noir'),
    ('Fondant'),
    ('Craquant');

INSERT INTO Accessoire (Label) 
VALUES 
    ('Boite en or'),
    ('Coffret anniversaire');

INSERT INTO Assortissement (Label, Description, UrlImage, Prix) 
VALUES 
    ('Les petites meringues', 'Un ensemble de 25 meringues...', '/img/pack/25-meringues-noir.jpeg', 25),
    ('Chocolats en Fleur', 'Un bouquet haut en couleurs et en saveurs !', '/img/pack/chocolats-en fleur.jpeg', 15),
    ('Pochette bleue organdi', 'Plein de chocolats', '/img/pack/pochette-bleue-organdi.jpeg', 30),
    ('Pochette marron organdi', 'Encore du chocolat', '/img/pack/pochette-marron-organdi.jpeg', 30),
    ('Les orangettes', 'Des orangettes en pagaille', '/img/pack/sachet fenetre orangettes-noir.jpeg', 20);


/* INSERTION DES JOINTURES */
INSERT INTO ChocolatCategorie (IdChocolat, IdCategorie) 
VALUES 
    /* Chocolats blancs */
    (3, 1),
    (10, 1),
    (15, 1),
    (16, 1),
    (17, 1),
    (18, 1),
    (23, 1),
    (29, 1),
    /* Chocolats au lait */
    (4, 2),
    (7, 2),
    (8, 2),
    (9, 2),
    (11, 2),
    (13, 2),
    (14, 2),
    (21, 2),
    (24, 2),
    (26, 2),
    (30, 2),
    /* Chocolats noirs */
    (1, 3),
    (2, 3),
    (5, 3),
    (6, 3),
    (12, 3),
    (19, 3),
    (20, 3),
    (22, 3),
    (25, 3),
    (27, 3),
    (28, 3),
    /* Fondant */
    (3, 4),
    (7, 4),
    (11, 4),
    (18, 4),
    (27, 4),
    /* Craquant */
    (2, 5),
    (5, 5),
    (8, 5),
    (15, 5),
    (20, 5),
    (21, 5);

INSERT INTO ChocolatAccessoire (IdChocolat, IdAccessoire) 
VALUES 
    (2, 1),
    (5, 1),
    (9, 1),
    (17, 1),
    (24, 1),
    (25, 1),
    (3, 2),
    (8, 2),
    (9, 2),
    (15, 2),
    (19, 2),
    (27, 2);

INSERT INTO ChocolatAssortissement (IdChocolat, IdAssortissement, Quantite) 
VALUES 
    (6, 1, 5),
    (9,1, 5),
    (22, 1,5),
    (25, 1, 5),
    (26, 1, 5),
    (4, 2, 3),
    (8, 2, 10),
    (12, 2, 5),
    (7, 3,4),
    (20 ,3,4),
    (22 ,3,2),
    (24 ,3,6),
    (1, 4, 4),
    (10, 4, 4),
    (11, 4, 2),
    (12, 4, 6),
    (6, 5, 15),
    (18, 5, 15);


/* Requetes tests : Retrouve les chocolats par catégories */
SELECT Chocolat.Nom
FROM (
  (Chocolat INNER JOIN ChocolatCategorie ON Chocolat.IdChocolat = ChocolatCategorie.IdChocolat)
  INNER JOIN Categorie ON Categorie.IdCategorie = ChocolatCategorie.IdCategorie);