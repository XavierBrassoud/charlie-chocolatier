var server = require('../app');

exports.start = function (req, res) {

    var isError = true;

    // On récupère les packs
    server.dbManager.getPacks()
        .then(packs => {
            isError = false;
            res.render('packs.html', { isError, packs });
        })
        .catch(error => {
            res.render('packs.html', { isError, error });
        })
}
