var server = require('./../app');

exports.start = function (req, res) {


    var chocolats = [];
    var chocolatsFiltre = [];
    var categoriesFiltre = [];

    var isError = true;

    // On récupère tous les chocolats
    server.dbManager.getChocolats('')
        .then(chocos => {
            
            chocolats = chocos;

            // GESTION DES CATÉGORIES
            if (req.query.cat) {
                

                if (typeof req.query.cat === 'string'){
                    categoriesFiltre.push(req.query.cat);
                }

                // Pour toutes les catégories
                categoriesFiltre.forEach((categorie) => {

                    // Regarder parmi tous les chocolats
                    chocolats.forEach((choco) => {

                        // Lesquels sont de la catégorie passée en paramètres
                        choco.Categories.forEach((categ) => {
                            if (categorie === categ) {
                                chocolatsFiltre.push(choco);
                            }
                        });
                    });
                });
            }
            else {
                // Si aucuns filtres alors on regarde tous les chocolats
                chocolatsFiltre = chocolats;
            }


            // GESTION DES PAGES
            var pages = [];
            var pageActuel = 1;
            var maxPage = 1;

            var numPage = 1;
            chocolatsFiltre.forEach((elem, index) => {

                // 10 items par pages
                if (index % 10 === 0) {
                    pages.push(numPage);
                    maxPage = numPage;
                    numPage = numPage + 1;
                }
            });


            // Instanciation de la page actuel
            if (req.query.page) {
                pages.forEach(page => {
                    if (req.query.page > page) {
                        pageActuel = 1;
                    }
                    else {
                        pageActuel = parseInt(req.query.page);
                    }
                });
            }

            chocosPage = [];
            chocolatsFiltre.forEach((elem, index) => {

                // 10 items par pages
                if ((pageActuel - 1) * 10 <= index && pageActuel * 10 > index) {
                    chocosPage.push(elem);
                }
            });

            
            // Les paramètres de catégorie passé à l'URL
            var catParams = '';

            categoriesFiltre.forEach((categ) => {
                catParams = catParams + '&cat=' + categ;
            });
            
            isError = false;
            res.render('produits.html', { isError, chocosPage, pages, pageActuel, maxPage, catParams });
        })
        .catch(error => {
            res.render('produits.html', { isError, error });
        })

}