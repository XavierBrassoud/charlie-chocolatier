var server = require('./../app');

exports.start = function (req, res) {

    var isError = true;

    // On récupère les chocolats favoris
    server.dbManager.getChocolats('WHERE Chocolat.EstFavori = true;')
        .then(chocolats => {
            isError = false;
            res.render('accueil.html', { isError, chocolats });
        })
        .catch(error => {
            res.render('accueil.html', { isError, error });
        })
}

