var express = require('express');
var swig = require('swig');
var DbManager = require('./services/dbmanager');


const N_PORT = 3001;
const N_LISTEN = 'localhost';


/* MIDDLEWARE */
const app = express();

app.engine('html', swig.renderFile);

app.set('view engine', 'html');
app.set('views', __dirname + '/views');

app.set('view cache', false);

swig.setDefaults({ cache: false });


var dbConfig = {
  host     : '192.168.99.100',
  user     : 'root',
  password : 'password123',
  database : 'chocolaterie',
  charset : 'UTF8_GENERAL_CI'
};

// Manager SQL accessible par les controllers
const dbManager = new DbManager(dbConfig);
module.exports = {dbManager};


// ROUTES DYNAMIQUES
var accueil = require('./controllers/accueil');
app.get('/', accueil.start);
app.get('/accueil', accueil.start);

var produits = require('./controllers/produits');
app.get('/produits', produits.start);

var packs = require('./controllers/packs');
app.get('/packs', packs.start);

app.get('/contact', (req, res) => res.render('contact.html', { }));

app.get('/propos', (req, res) => res.render('propos.html', { }));



/* ROUTES STATIQUES */
// Styles
app.use('/css/bootstrap.min.css', express.static('node_modules/bootstrap/dist/css/bootstrap.min.css'));
app.use('/css/', express.static('views/styles/'));

// Images
app.use('/img/individuel/', express.static('views/img/individuel/'));
app.use('/img/pack/', express.static('views/img/pack/'));

// Scripts
app.use('/js/bootstrap.min.js', express.static('node_modules/bootstrap/dist/js/bootstrap.min.js'));
app.use('/js/jquery.slim.js', express.static('node_modules/jquery/dist/jquery.slim.js'));
app.use('/js/popper.min.js', express.static('node_modules/popper.js/dist/popper.min.js'));



// Démarrage du serveur
app.listen(N_PORT);
console.log('[server] Application started on http://'+ N_LISTEN + ':' + N_PORT);

