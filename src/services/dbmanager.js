var mysql = require('mysql');

/** Classe de gestion de la BDD de la Chocolaterie */
module.exports = class DbManager {

    constructor(config) {
        this.config = config;
        this.connection = mysql.createConnection(this.config);
    }

    // Requêtes SQL Asynchrones (promesses)
    query(sql, args) {

        // Reconnexion automatique
        if (this.connection.state === 'disconnected') {
            this.connection = mysql.createConnection(this.config);
        }

        return new Promise((resolve, reject) => {
            this.connection.query(sql, args, (err, rows) => {
                if (err)
                    return reject(err);
                resolve(rows);
            });
        });
    }

    // Fermeture de la session à la BDD (promesses)
    close() {
        return new Promise((resolve, reject) => {
            this.connection.end(err => {
                if (err)
                    return reject(err);
                resolve();
            });
        });
    }

    // Récupère tous les chocolats avec leurs catégories et leurs accessoires
    getChocolats(condition) {
        return new Promise((resolve, reject) => {

            // Tableau qui va servir à stocker nos chocolats
            var chocolats;

            // Récupération de tous les chocolats en BDD
            this.query(
                `SELECT Chocolat.IdChocolat, Chocolat.Nom, Chocolat.Description, Chocolat.UrlImage, Chocolat.Prix, Chocolat.Reduction
             FROM Chocolat ` + condition + ';')
                .then(results => {
                    chocolats = results

                    // Récupération des catégories associés au Chocolats précédemment récupérés
                    return this.query(
                        `SELECT ChocolatCategorie.IdChocolat, Categorie.Label
                     FROM Categorie 
                     JOIN ChocolatCategorie ON Categorie.IdCategorie = ChocolatCategorie.IdCategorie`
                    );
                })
                .then(results => {

                    // Mappage des catégories dans les chocolats correspondant
                    chocolats.forEach(function (choco, index) {
                        chocolats[index].Categories = [];
                        results.forEach(function (cat) {
                            if (cat.IdChocolat === choco.IdChocolat) {
                                chocolats[index].Categories.push(cat.Label);
                            }
                        });
                    });

                    // Récupération des accessoires associés au Chocolats précédemment récupérés
                    return this.query(
                        `SELECT ChocolatAccessoire.IdChocolat, Accessoire.Label
                     FROM Accessoire 
                     JOIN ChocolatAccessoire ON Accessoire.IdAccessoire = ChocolatAccessoire.IdAccessoire`
                    );

                })
                .then(results => {
                    
                    // Mappage des accessoires dans les chocolats correspondant
                    chocolats.forEach(function (choco, index) {
                        chocolats[index].Accessoires = [];
                        results.forEach(function (cat) {
                            if (cat.IdChocolat === choco.IdChocolat) {
                                chocolats[index].Accessoires.push(cat.Label);
                            }
                        });
                    });

                    // Fermeture de la connexion à la BDD
                    return this.close();
                }, err => {
                    // Si erreur : On tente une fermeture, si encore échec on soulève une exception
                    return this.close().then(() => { throw err; })
                })
                .then(() => {

                    // Si tous s'est bien passé, on retourne le tableau d'objets chocolats mappés
                    resolve(chocolats);
                })
                .catch(err => {
                    // Sinon on retourne l'erreur capturée
                    reject(err);
                })
        });
    }

    // Récupère tous les packs avec leurs quantité de chocolats
    getPacks() {
        return new Promise((resolve, reject) => {

            // Tableau qui va servir à stocker nos chocolats
            var packs;

            // Récupération de tous les packs en BDD
            this.query(
                `SELECT Assortissement.IdAssortissement, Assortissement.Label, Assortissement.Description, Assortissement.UrlImage, Assortissement.Prix
                FROM Assortissement;`)
                .then(results => {
                    packs = results

                    // Récupération des quantités de chocolats
                    return this.query(
                        `SELECT ChocolatAssortissement.IdAssortissement, ChocolatAssortissement.Quantite, Chocolat.Nom
                            FROM Chocolat 
                            JOIN ChocolatAssortissement ON Chocolat.IdChocolat = ChocolatAssortissement.IdChocolat`
                    );
                })
                .then(results => {

                    // Mappage des quantités de chocolat aux packs
                    packs.forEach(function (pack, index) {
                        packs[index].Chocolats = [];
                        results.forEach(function (choco) {
                            if (pack.IdAssortissement === choco.IdAssortissement) {
                                packs[index].Chocolats.push({nom: choco.Nom, quantite: choco.Quantite});
                            }
                        });
                    });
                    // Fermeture de la connexion à la BDD
                    return this.close();
                }, err => {
                    // Si erreur on ferme la connexion à la BDD
                    return this.close().then(() => { throw err; })
                })
                .then(() => {
                    // Si tous va bien on retourne le tableau d'objets packs mappé
                    resolve(packs);
                })
                .catch(err => {
                    // sinon on retourne l'erreur capturée
                    reject(err);
                })
        });
    }
}
